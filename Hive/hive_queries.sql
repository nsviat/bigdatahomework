CREATE DATABASE test_shema;
CREATE DATABASE test_shema2 LOCATION '/user/hive';
SHOW DATABASES;
SHOW DATABASES LIKE '*2*';
DROP DATABASE TEST_SCHEMA2;

USE test_shema;
CREATE TABLE table_1 (id Int, name String);
INSERT INTO table_1 VALUES (1, 'aaa'), (2, 'bbb');
SELECT * FROM table_1;

ALTER TABLE table_1 ADD COLUMN (type Int);  CREATE TABLE table_temp AS SELECT * FROM table_1;  
	     DROP TABLE table_1;  CREATE TABLE table_1 AS SELECT id, type, name FROM table_temp;
SELECT * FROM table_1;

INSERT INTO TABLE table_1 VALUES (3, 2, 'ccc');
SELECT * FROM table_1;

CREATE TABLE table_temp AS SELECT * FROM table_1 WHERE id < 1;
ALTER TABLE table_temp CLUSTERED BY (type) INTO 10 BUCKETS;
INSERT INTO table table_temp select * from table_1;
DROP TABLE table_1;
ALTER TABLE table_temp RENAME TO table_1;
SELECT * FROM table_1;

hdfs dfs -ls -R /warehouse/tablespace/managed/hive/test_shema.db/table_1
ALTER TABLE table_1 SET TBLPROPERTIES('EXTERNAL'='TRUE')

DESCRIBE FORMATTED table_1;
LOAD DATA INPATH '/warehouse/tablespace/managed/hive/test_shema.db/table_1' INTO TABLE table_2;

CREATE EXTERNAL  TABLE table_2 AS SELECT * FROM table_1;
SELECT * FROM table_2;

CREATE EXTERNAL TABLE table_temp(id int, name string) PARTITIONED BY (type int);
INSERT OVERWRITE TABLE table_temp PARTITION(type) SELECT id, type, name FROM table_1;
ALTER table table_temp RENAME TO table_2;
ALTER TABLE table_2 ADD COLUMNS (action_time date);
SELECT *  FROM table_2;

INSERT INTO table_2 VALUES (4, 'ddd', '2018-10-31', 2), (5, 'eee', '2019-11-01', 3);
SELECT *  FROM table_2;

SELECT *  FROM table_2 ORDER BY action_time;
SELECT *  FROM table_2 WHERE action_time > current_date();

SELECT *  FROM table_2 WHERE year(action_time) = 2019;
SELECT emp.type AS type, emp.id AS id, emp.name AS name FROM table_2 emp, table_2 manager WHERE emp.type = manager.type ORDER BY emp.name;
SELECT type, FIRST_VALUE(id) over (partition by type) as id, FIRST_VALUE(name) over (partition by type) as name
					, FIRST_VALUE(action_time) over (PARTITION BY type) AS first_time from table_1 order by name asc;

SHOW PARTITIONS table_2;
DESC table_2;
DESC FORMATTED table_2;
ALTER TABLE table_2 DROP PARTITION(type!=2);
SELECT * FROM table_2;

CREATE TABLE table_3 AS SELECT * FROM table_2 STORED AS ORC LOCATION '/warehouse/t' TBLPROPERTIES ('ORC.COMPRESS'='SNAPPY');
SELECT * FROM table_3;

USE test_shema;
SHOW TABLES;
SHOW TABLES LIKE '*3*';

DROP DATABASE test_shema CASCADE;