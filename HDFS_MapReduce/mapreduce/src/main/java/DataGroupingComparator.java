import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class DataGroupingComparator extends WritableComparator {
    public DataGroupingComparator(){
        super(DataPair.class, true);
    }

    public int compare(WritableComparable wc1, WritableComparable wc2) {
        DataPair pair = (DataPair) wc1;
        DataPair pair2 = (DataPair) wc2;

        return pair.getId().compareTo(pair2.getId())*-1;
    }
}
