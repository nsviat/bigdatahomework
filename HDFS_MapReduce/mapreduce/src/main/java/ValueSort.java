import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.log4j.BasicConfigurator;

import java.io.File;

import static org.eclipse.jetty.util.IO.delete;

public class ValueSort {
    public static void main(String[] args) throws Exception {
        BasicConfigurator.configure();

        Path inputPath = new Path("src/main/resources/hotels/part1.csv");
        Path outputDir = new Path("src/main/resources/sorted_hotels.csv");

        // Create job
        JobConf job = new JobConf(ValueSort.class);
        job.setJarByClass(ValueSort.class);

        // Setup MapReduce
        job.setMapperClass(MapTask.class);
//        job.setReducerClass(ReduceTask.class);
        job.setNumReduceTasks('1');
        job.setCombinerKeyGroupingComparator(DataGroupingComparator.class);
        job.setPartitionerClass(DataPartitioner.class);

        // Specify key / value
        job.setMapOutputKeyClass(LongWritable.class);
        job.setMapOutputValueClass(Text.class);
        job.setOutputKeyClass(LongWritable.class);
        job.setOutputValueClass(Text.class);

        // Input
        FileInputFormat.setInputPaths(job, inputPath);
        job.setInputFormat(TextInputFormat.class);

        // Output
        FileOutputFormat.setOutputPath(job, outputDir);
        job.setOutputFormat(TextOutputFormat.class);

        File directory = new File("src/main/resources/sorted_hotels.csv");
        delete(directory);

        JobClient.runJob(job);
    }
}
