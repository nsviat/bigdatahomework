import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.Partitioner;

public class DataPartitioner implements Partitioner<LongWritable, Text> {

    @Override
    public int getPartition(LongWritable dataPair, Text text, int numberOfPartitions) {
        return Math.abs(dataPair.hashCode() % 1);
    }

    @Override
    public void configure(JobConf jobConf) {

    }
}
