import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

import java.io.IOException;

public class MapTask extends MapReduceBase implements
        Mapper<LongWritable, Text, LongWritable, Text> {

    @Override
    public void map(LongWritable key, Text value, OutputCollector<LongWritable, Text> output, Reporter reporter) throws IOException {
        String line = value.toString();
        String[] tokens = line.split(",", 2);

        LongWritable id = new LongWritable(Long.parseLong(tokens[0]));
        Text valuePart = new Text(tokens[1]);

        DataPair data = new DataPair();
        data.setId(id);
        data.setInfo(valuePart);

        output.collect(data.getId(), valuePart);
    }
}