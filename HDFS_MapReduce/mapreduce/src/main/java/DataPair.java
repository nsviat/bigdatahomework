import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class DataPair implements Writable, WritableComparable<DataPair> {
    private LongWritable id = new LongWritable();
    private Text info = new Text();

    public LongWritable getId() {
        return id;
    }

    public Text getInfo() {
        return info;
    }

    public void setId(LongWritable id) {
        this.id = id;
    }

    public void setInfo(Text info) {
        this.info = info;
    }

    @Override
    public int compareTo(DataPair pair) {
        int compareValue = this.id.compareTo(pair.getId());

        if (compareValue == 0)
            compareValue = 1;

        return compareValue*-1;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {

    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {

    }
}
