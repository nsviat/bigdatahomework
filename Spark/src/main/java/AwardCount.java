import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.*;
import scala.Tuple2;

public final class AwardCount {
    public static void main(String[] args) throws Exception {
        String[] files = {"D:\\IT\\Epam\\bigdatahomework\\Spark\\src\\main\\resources\\AwardsPlayers.csv",
                "D:\\IT\\Epam\\bigdatahomework\\Spark\\src\\main\\resources\\Master.csv"};

        SparkSession spark = SparkSession
                .builder().master("local")
                .appName("JavaRDD")
                .getOrCreate();

        //load files
        JavaRDD<String> awardPlayers = spark.read().textFile(files[0]).javaRDD();
        JavaRDD<String> playerInfo = spark.read().textFile(files[1]).javaRDD();

        //parse playerID and award for playerID
        JavaRDD<String> awards = awardPlayers.map(x -> {
            String[] words = x.split(",");
            return words[0] + "," + words[1];
        });

        //converting to (key, value) key - playerID, value - 1 for each playerID
        JavaPairRDD<String, Integer> pairAwards = awards.mapToPair(x -> {
            String[] words = x.split(",");
            return new Tuple2<>(words[0], 1);
        });

        //get number of awards for each playerID
        JavaPairRDD<String, Integer> groupedAwards = pairAwards.reduceByKey(Integer::sum);

        //parse playerID and first & last name for playerID
        JavaRDD<String> players = playerInfo.map(x -> {
            String[] words = x.split(",");
            return words[0] + "," + words[3] + " " + words[4];
        });

        //converting to (key, value) key - playerID, value - first and last name for each playerID
        JavaPairRDD<String, String> pairPlayers = players.mapToPair(x -> {
            String[] words = x.split(",");
            return new Tuple2<>(words[0], words[1]);
        });

        //join pairPlayers and groupedAwards by key
        JavaPairRDD<String, Tuple2<Integer, String>> playersAndAwards = groupedAwards.join(pairPlayers);

        //swap columns
        JavaRDD<String> countPlayerAwards = playersAndAwards.map(x -> x._2).map(x -> {
            return (x._2 + " -  " + x._1 + "\n");
        });

        countPlayerAwards.coalesce(1).saveAsTextFile("src/main/java/resources/rdd_output.csv");

        spark.stop();
    }
}