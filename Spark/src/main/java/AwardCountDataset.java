import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;

public class AwardCountDataset {
    public static void main(String[] args) throws Exception {
        String[] files = {"D:\\IT\\Epam\\bigdatahomework\\Spark\\src\\main\\resources\\AwardsPlayers.csv",
                "D:\\IT\\Epam\\bigdatahomework\\Spark\\src\\main\\resources\\Master.csv"};

        SparkSession spark = SparkSession
                .builder().master("local")
                .appName("JavaQuery")
                .getOrCreate();

        //load file with awards
        Dataset<Row> awardPlayers = spark.read().csv(files[0]);
        //selec tneeded columns and add reward counter
        Dataset<Row> awards = awardPlayers.select("_c0")
                .withColumnRenamed("_c0", "playerID")
                .withColumn("count_awards", functions.lit(1));
        awards.createOrReplaceTempView("awardss");
        //grouping
        //Dataset<Row> awardsCounted = awards.groupBy("playerID").count();
        Dataset<Row> awardsCounted = awards.sqlContext().sql("SELECT playerID, SUM(count_awards) " +
                "AS count FROM awardss GROUP BY playerID");
        awardsCounted.createOrReplaceTempView("awards");

        //load file with players
        Dataset<Row> playerInfo = spark.read().csv(files[1]);
        //select needed columns
        Dataset<Row> players = playerInfo.select("_c0", "_c3", "_c4")
                .withColumnRenamed("_c0", "playerID")
                .withColumnRenamed("_c3", "first_name")
                .withColumnRenamed("_c4", "last_name");
        players.createOrReplaceTempView("players");
        //aggregate first and last name
        players = players.sqlContext().sql("SELECT playerID, CONCAT(first_name, ' ', last_name) as full_name FROM players");
        players.createOrReplaceTempView("players");

        //join two datasets
        Dataset<Row> joined = awardsCounted.sqlContext().sql("SELECT * FROM awards CROSS JOIN players ON " +
                "awards.playerID=players.playerID");

        //save result in file
        joined.coalesce(1).write().csv("src/main/java/resources/sql_output.csv");

        spark.stop();
    }
}
