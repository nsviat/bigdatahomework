package SparkStreaming;


import org.apache.commons.math3.stat.inference.TestUtils;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.spark.TestUtils$;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.streaming.StreamingQuery;
import org.apache.spark.sql.streaming.StreamingQueryException;
import org.apache.spark.sql.streaming.Trigger;
import org.apache.spark.storage.StorageLevel;

import java.util.Arrays;
import java.util.Properties;
import java.util.regex.Pattern;

import static org.apache.spark.sql.functions.*;



/*for cheking avro schema
java -jar avro-tools-1.7.7.jar tojson src/main/java/resources/expedia/part-00001-ef2b800c-0702-462d-b37f-5f2fb3a093d0-c000.avro >  src/main/java/resources/smth.json*/


public class SparkStreaming {
    public static void main(String[] args) throws StreamingQueryException, InterruptedException {
        String[] files = {"D:\\IT\\Epam\\bigdatahomework\\Spark\\src\\main\\resources\\weather",
                "D:\\IT\\Epam\\bigdatahomework\\Spark\\src\\main\\resources\\expedia",
                "D:\\IT\\Epam\\bigdatahomework\\Spark\\src\\main\\resources\\hotels"};

        SparkSession spark = SparkSession
                .builder().master("local")
                .appName("JavaStreaming")
                .getOrCreate();

//        read expedia
        Dataset<Row> expedia2016 = spark.read().format("avro")
                .load(files[1]).where("srch_ci LIKE '%2017%'");
        expedia2016.createOrReplaceTempView("xx");

        Dataset<Row> expedia2017 = spark.read().format("avro")
                .load(files[1]).where("srch_ci LIKE '%2016%'");
        expedia2017.createOrReplaceTempView("xxx");

        Dataset<Row> expedia = expedia2016.union(expedia2017).drop("date_time");
        expedia.createOrReplaceTempView("expedia");
        //============================================

        //read weather
        Dataset<Row> weather2016 = spark.read().format("parquet")
                .load(files[0]).drop("year", "month", "day")
                .filter((FilterFunction<Row>) x -> x.getAs("wthr_date")
                        .toString().contains("2016"));
        weather2016.createOrReplaceTempView("weather2016");
        Dataset<Row> weather2017 = spark.read().format("parquet")
                .load(files[0]).drop("year", "month", "day")
                .filter((FilterFunction<Row>) x -> x.getAs("wthr_date")
                        .toString().contains("2017"));
        weather2017.createOrReplaceTempView("weather2017");
        //============================================

        //read hotels
        Dataset<Row> hotels = spark.read().csv(files[2]).select("_c0", "_c1", "_c2");
        hotels.createOrReplaceTempView("hotels");
        //============================================

        Dataset<Row> weather = weather2016.union(weather2017);
        weather.createOrReplaceTempView("weather");

        Dataset<Row> avgTemp = spark.sqlContext().sql("SELECT wthr_date, AVG(avg_tmpr_f) AS avg_f, AVG(avg_tmpr_c) AS avg_c " +
                "FROM weather GROUP BY wthr_date");
        avgTemp.createOrReplaceTempView("avgTemp");

        Dataset<Row> checkInWeather = avgTemp.join(expedia, avgTemp.col("wthr_date").equalTo(expedia.col("srch_ci")));
        checkInWeather.createOrReplaceTempView("checkInWeather");

        Dataset<Row> weatherHotelsCheckin = checkInWeather.join(hotels, checkInWeather
                .col("hotel_id").equalTo(hotels.col("_c0")))
                .filter((FilterFunction<Row>) x -> (Double) x.getAs("avg_c") > 0);
        weatherHotelsCheckin.createOrReplaceTempView("weatherHotelsCheckin");

        Dataset<Row> stay = spark.sql("SELECT *, DATEDIFF(srch_co, srch_ci) AS stay, " +
                "CASE WHEN DATEDIFF(srch_co, srch_ci) BETWEEN 1 AND 3 THEN 'Short stay' " +
                "WHEN DATEDIFF(srch_co, srch_ci) BETWEEN 4 AND 7 THEN 'Standart stay' " +
                "WHEN DATEDIFF(srch_co, srch_ci) BETWEEN 8 AND 14 THEN 'Standart extended stay' " +
                "WHEN DATEDIFF(srch_co, srch_ci) BETWEEN 15 AND 28 THEN 'Long stay' " +
                "ELSE 'Can not classify' END as StayProps, " +
                "CASE WHEN srch_children_cnt = 0 OR NULL THEN 'NO' " +
                "ELSE 'YES' END AS Childs FROM weatherHotelsCheckin")
                .drop("_c0").withColumnRenamed("_c1", "hotel_name")
                .withColumnRenamed("_c2", "hotel_country");
//        2,5
        stay.show();

    }
}


