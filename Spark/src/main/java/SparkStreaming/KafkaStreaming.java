package SparkStreaming;

import java.util.*;


import org.apache.kafka.common.serialization.StringDeserializer;


public class KafkaStreaming {
    static final HashMap kafkaParams = new HashMap();
    static final Collection<String> topics = Arrays.asList("topicA", "topicB");

    static Map getKafkaParams() {
        if (kafkaParams.size() == 0) {
            kafkaParams.put("bootstrap.servers", "localhost:9092,anotherhost:9092");
            kafkaParams.put("key.deserializer", StringDeserializer.class);
            kafkaParams.put("value.deserializer", StringDeserializer.class);
            kafkaParams.put("group.id", "use_a_separate_group_id_for_each_stream");
            kafkaParams.put("auto.offset.reset", "latest");
            kafkaParams.put("enable.auto.commit", false);
        }
        return kafkaParams;
    }
}
