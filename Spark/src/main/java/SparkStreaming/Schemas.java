package SparkStreaming;

import org.apache.spark.sql.types.StructType;

public class Schemas {
    static final StructType expedia_schema = new StructType()
            .add("id", "long")
            .add("date_time", "String")
            .add("site_name", "integer")
            .add("posa_continent", "integer")
            .add("user_location_country", "integer")
            .add("user_location_region", "integer")
            .add("user_location_city", "integer")
            .add("orig_destination_distance", "double")
            .add("user_id", "integer")
            .add("is_mobile", "integer")
            .add("is_package", "integer")
            .add("channel", "integer")
            .add("srch_ci", "string")
            .add("srch_co", "string")
            .add("srch_adults_cnt", "integer")
            .add("srch_children_cnt", "integer")
            .add("srch_rm_cnt", "integer")
            .add("srch_destination_id", "integer")
            .add("srch_destination_type_id", "integer")
            .add("hotel_id", "long");

}
